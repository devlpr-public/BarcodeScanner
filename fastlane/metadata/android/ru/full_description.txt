Barcode Scanner - это свободное приложение с открытым исходным кодом, которое позволяет считывать и генерировать штрих-коды. С его помощью можно собирать информацию о продуктах питания, косметике и книгах.

Приложение поддерживает различные форматы штрих-кодов :
• Двухмерные(2D) штрих-коды: QR Code, Data Matrix, PDF 417, AZTEC
• Линейные(1D) штрих-коды: EAN 13, EAN 8, UPC A, UPC E, Code 128, Code 93, Code 39, Codabar, ITF

Сбор информации о продукте при сканировании :
• Продукты питания из Open Food Facts
• Косметика из Open Beauty Facts
• Продукты питания для домашних животных из Open Pet Food Facts
• Книги из Open Library
• Music CDs, Vinyls… with MusicBrainz

Возможности приложения :
• Просто наведите камеру смартфона на штрих-код и моментально получите информацию о нем. Также вы можете сканировать штрих-коды по изображению из вашего смартфона.
• С помощью обычного сканирования можно читать визитные карточки, добавлять новые контакты, вносить новые события в повестку дня, открывать URL-адреса или даже подключаться к Wi-Fi.
• Сканируйте штрих-коды товаров для получения информации об их составе из баз данных Open Food Facts и Open Beauty Facts.
• Ищите информацию о товаре, который вы сканируете, с помощью быстрого анализа на различных веб-сайтах, таких как Amazon или Fnac.
• Отслеживайте все сосканированные штрих-коды в истории.
• Создавайте собственные штрих-коды
• Настраивайте интерфейс с помощью различных цветов, светлой или темной темы. В приложение интегрированы возможности Android 12, позволяющие настраивать цвета в зависимости от ваших обоев.
• Приложение полностью переведено на английский, испанский, французский, немецкий, польский, турецкий, русский и китайский языки.

Это приложение уважает вашу приватность. Оно не содержит никаких трекеров и не собирает никаких данных.
